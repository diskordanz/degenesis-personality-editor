export const messages = {
  de: {
    locale: 'Sprache',
    name: 'Name',
    age: 'Alter',
    rank: 'Rang',
    experience: 'Erfahrung',
    gender: 'Geschlecht',
    height: 'Größe',
    weight: 'Gewicht',
    dinars: 'Dinar/Wechsel',
    trauma: 'Trauma',
    fleshwounds: 'Fleischwunden',
    ego: 'Ego',
    sporeInfestations: 'Versporung',
    attributePoints: 'Attributspunkte',
    skillPoints: 'Fertigkeitspunkte',
    originPoints: 'Hintergrundpunkte',
    potentialPoints: 'Potenzialpunkte',
    origins: 'Hintergründe',
    potentials: 'Potenziale',
    concept: 'Konzept',
    culture: 'Kultur',
    cult: 'Kult',
    clan: 'Sippe',
    characterName: 'Name',
    saveCharacter: 'speichern',
    loadCharacter: 'Laden',
    deleteCharacter: 'löschen',
    confirmCharacterDeletion: 'Charakter "{name}" permanent löschen?',
    characterSheet: 'Charakterbogen',
    generateSheet: 'PDF erzeugen',
    sheetOptions: 'Charakterbogenoptionen',
    preferences: {
      label: 'Einstellungen',
      displayTranslatedLabelsDescription: 'Deutsche Übersetzungen für Begriffe ohne offizielle Übersetzung anzeigen?',
      untranslatedLabels: 'Bezeichner',
      displayTranslatedLabels: 'Übersetzungen',
      displayOriginalLabels: 'Original',
    },
    characters: 'Charaktere',
    exportCharacter: 'Exportieren',
    importCharacter: 'Charakter importieren',
    editorModes: {
      label: 'Bearbeitungsmodus',
      hardLimits: 'Strikt',
      softLimits: 'Normal',
      free: 'Frei',
      hints: {
        hardLimits: 'Punktelimits können nicht überschritten werden.',
        softLimits: 'Punktelimits können überschritten werden. Überschreitungen werden markiert.',
        free: 'Punktelimits können überschritten werden'
      },
      hintHardLimitModeImpossible: 'Der strikte Bearbeitungsmodus kann nicht gewählt werden, wenn bereits eines der Punktelimits überschritten wurde.'
    },
    hint: 'Hinweis',
    buildPoints: 'Generierungspunkte',
    buildOptions: 'Generierungsoptionen',
    editCharacter: 'Editieren',
    createNewCharacter: 'Neuer Charakter',
    cancel: 'Abbrechen',
    close: 'Schließen',
    characterAlreadyExists:
      'Ein Charakter mit diesem Namen existiert bereits. Bitte wähle einen anderen Namen.',
    noPrerequisites: 'Keine Voraussetzungen',
    rename: 'Umbenennen',
    renameCharacter: 'Charakternamen ändern',
    createRenamedCopy: 'Kopie erstellen',
    welcome: 'Willkommen in Noret',
    introduction: {
      whatIsIt: 'Was ist diese Seite?',
      whatIsItText1:
        'Dies ist ein Charaktereditor für Katharsys, das Regelwerk für das Rollenspiel Degenesis. ' +
        'Du kannst diese Applikation verwenden, um neue Charaktere zu erstellen, sowie um bereits erstellte ' +
        'anzuschauen und zu editieren. Für jeden Charakter kannst du eine Vorschau des Charakterbogens ' +
        'erzeugen, und den Bogen dann als ein druckbares PDF exportieren.',
      whatIsItText2:
        'Jeder deiner Charaktere, sowie alle Änderungen an diesen, werden lokal in deinem Browser gespeichert. ' +
        'Dies bedeutet, dass du jedes mal, wenn du diese Seite mit dem gleichen Browser besuchts, alle deine ' +
        'erstellten Charaktere vorfinden wirst. Leider beduetet dies auch, dass du keine Links auf deine Charaktere ' +
        'erzeugen kannst, die andere Leute einsehen können. Du kannst jedoch jeden Charakter als eine Datei exportieren, ' +
        'um diese an Andere weiterzugeben. Mit dieser Datei können deine Mitspieler ebenfalls diese Seite besuchen, ' +
        'und deinen Charakter in ihren Browser importieren. Ich habe diese (zugegebenermaßen etwas umständliche) Funktionsweise ' +
        'gewählt, um den Aufwand des Betriebs dieser Seite minimal zu halten.',
      whatIsItText3:
        'Der Quellcode für diese Seite ist verfügbar bei {0}. Ich möchte diese Applikation für alle ' +
        'verfügbar halten, damit sie in der Zukunft auch von anderen Leuten betrieben werden kann. Ziel ist, damit ' +
        'eine Situation wie mit dem offiziellen Charaktereditor zu vermeiden, der in der Vergangenheit bereits für längere ' +
        'Zeit nicht verfügbar war.'
    }
  },
  en: {
    locale: 'language',
    name: 'Name',
    age: 'Age',
    rank: 'Rank',
    experience: 'Experience',
    gender: 'Gender',
    height: 'Height',
    weight: 'Weight',
    dinars: 'Dinars/Drafts',
    trauma: 'trauma',
    fleshwounds: 'fleshwounds',
    ego: 'ego',
    sporeInfestations: 'spore infestations',
    attributePoints: 'Attribute points',
    skillPoints: 'Skill points',
    originPoints: 'Origin points',
    potentialPoints: 'Potential points',
    origins: 'Origins',
    potentials: 'Potentials',
    concept: 'Concept',
    culture: 'Culture',
    cult: 'Cult',
    clan: 'Clan',
    characterName: 'Name',
    saveCharacter: 'Save',
    loadCharacter: 'Load',
    deleteCharacter: 'Delete',
    confirmCharacterDeletion: 'Permanently delete character "{name}"?',
    characterSheet: 'character sheet',
    generateSheet: 'Generate PDF',
    sheetOptions: 'Sheet Options',
    preferences: {
      label: 'Preferences',
      displayTranslatedLabelsDescription: 'Display translated labels (only relevant for non-english locale)?',
      untranslatedLabels: 'Labels',
      displayTranslatedLabels: 'Translated',
      displayOriginalLabels: 'Original',
    },
    characters: 'Characters',
    exportCharacter: 'Export',
    importCharacter: 'Import character',
    editorModes: {
      label: 'Editor mode',
      hardLimits: 'Strict',
      softLimits: 'Normal',
      free: 'Free',
      hints: {
        hardLimits: 'Point limits cannot be exceeded.',
        softLimits: 'Point limits can be exceeded. Any overages will be marked.',
        free: 'Point limits can be exceeded.'
      },
      hintHardLimitModeImpossible: 'The strict editor mode cannot be selected if any point limit has already been exceeded.'
    },
    hint: 'Hint',
    buildPoints: 'Build points',
    buildOptions: 'Build options',
    editCharacter: 'Edit',
    createNewCharacter: 'Create New',
    cancel: 'Cancel',
    close: 'Close',
    characterAlreadyExists:
      'A character with the given name already exists. Please choose another name.',
    noPrerequisites: 'No prerequisites',
    rename: 'rename',
    renameCharacter: 'Rename character',
    createRenamedCopy: 'Create copy',
    welcome: 'Welcome to Noret',
    introduction: {
      whatIsIt: 'What is this?',
      whatIsItText1:
        'This is a character editor for Katharsys, the rule' +
        'system used by the roleplaying game Degenesis. You can use this application to  create new characters as well as ' +
        'peruse and edit those you previously created. For any character, you can look at a preview ' +
        'of the character sheet. This character sheet can also be exported as a printable PDF file.',
      whatIsItText2:
        'Any of your characters, as well as all changes to them, are persisted to the local storage of your browser. ' +
        'This means any time you visit this page with the same browser, you will be able to see your ' +
        'characters in the navigation drawer on the left. Click on them to view and edit them. ' +
        'Unfortunately, since the characters are stored within your browser only, you cannot create ' +
        'links to your characters so that other people can see them. However, you can export any character. ' +
        'You can give this export file to someone else. This person can then also visit this page and ' +
        'import this file. I have chosen this (admittedly somewhat limiting) mode of operation to reduce the ' +
        'technical and legal overhead of running this site.',
      whatIsItText3:
        'The source code for this page is available at {0}. The intention is to make this application ' +
        'available to anyone wishing to run it for themselves. I want to prevent a similar situation as with the ' +
        'official generator, which has previously been unavailable for longer periods of time.'
    }
  }
}
