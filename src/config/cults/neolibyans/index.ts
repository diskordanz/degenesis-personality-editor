import { Cult } from '@/config/model'
import { Skills } from '../../properties'

export const Neolibyans = new Cult('neolibyans', [
  Skills.projectiles,
  Skills.conduct,
  Skills.cunning,
  Skills.negotiation,
  Skills.empathy
])
