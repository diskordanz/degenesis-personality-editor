import { Cult } from '../../model'
import { Skills } from '../../properties'

export const Anabaptists = new Cult('anabaptists', [
      Skills.melee,
      Skills.force,
      Skills.legends,
      Skills.mobility,
      Skills.faith,
      Skills.willpower
    ])
