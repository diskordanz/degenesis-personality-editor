import { Cult } from '@/config/model'
import { Skills } from '../../properties'

export const Apocalyptics = new Cult('apocalyptics', [
  Skills.athletics,
  Skills.dexterity,
  Skills.seduction,
  Skills.arts,
  Skills.cunning
])
