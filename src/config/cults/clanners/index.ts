import { Cult } from '@/config/model'

export const Clanners = new Cult(
  'clanners',
  [] // bonus skills are provided by the actual clan
)
