# Noret: Cryogenic Vaults

This is a character editor for Katharsys, the rulesystem used by the roleplaying game [Degenesis][degenesis]. You can use this application to create new characters as well as peruse and edit those you previously created. For any character, you can look at a preview of the character sheet. This character sheet can also be exported as a printable PDF file.

Any of your characters, as well as all changes to them, are persisted to the local storage of your browser. This means any time you visit this page with the same browser, you will be able to see your characters in the navigation drawer on the left. Click on them to view and edit them. Unfortunately, since the characters are stored within your browser only, you cannot create links to your characters so that other people can see them. However, you can export any character. You can give this export file to someone else. This person can then also visit this page and import this file. I have chosen this (admittedly somewhat limiting) mode of operation to reduce the technical and legal overhead of running _Noret_.

## Demo

The [beta instance](https://beta.noret.de) is behind a primitive password protection so that it counts as a private page and is not subject to German imprint law. Ask `@diskordanz` in the Degenesis discord for the password, if you're interested.

## Development

Working on this application requires the following:

- `nodejs` 18.x
- `npm`

An order to get to work, run:

```
npm install
```

You can run the tests with

```
npm run test:unit
```

and trigger a production build with

```
npm run build
```

## Running locally

The local development server may be started with

```
npm run dev
```

## Copyright notice

_Noret_ is a community creation for the tabletop role playing game [Degenesis®][degenesis]. Degenesis® is a trademark of [SIXMOREVODKA® Studio GmbH][smv]. All rights reserved. This project is not affiliated with SIXMOREVODKA Studio GmbH.

[degenesis]: https://degenesis.com
[smv]: https://www.sixmorevodka.com